part of 'family_bloc.dart';

abstract class FamilyState extends Equatable {
  const FamilyState();
}

class LoadingGetDataFamilyState extends FamilyState {
  @override
  List<Object> get props => [];
}

class SuccessGetDataFamilyState extends FamilyState {
  final List<FamilyModel> data;

  SuccessGetDataFamilyState(this.data);
  @override
  List<Object> get props => [this.data];
}

class FailedGetDataFamilyState extends FamilyState {
  final String error;

  FailedGetDataFamilyState(this.error);
  @override
  List<Object> get props => [this.error];
}

class LoadingPostDataFamilyState extends FamilyState {
  @override
  List<Object> get props => [];
}

class SuccessPostDataFamilyState extends FamilyState {
  
  @override
  List<Object> get props => [];
}

class FailedPostDataFamilyState extends FamilyState {
  final String error;

  FailedPostDataFamilyState(this.error);
  @override
  List<Object> get props => [this.error];
}
