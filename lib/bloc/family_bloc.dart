import 'package:family_tree/models/family_model.dart';
import 'package:family_tree/services/family_service.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';

part 'family_state.dart';

class FamilyBloc extends Cubit<FamilyState> {
  FamilyService _service = FamilyService();
  FamilyBloc() : super(null);
  
  void getDataFamily()async{
    final _list = List<FamilyModel>();
    emit(LoadingGetDataFamilyState());

    try {
      final data = await _service.readDataFamily();
      data.forEach((v){
        var familyModel = FamilyModel();
        familyModel.id = v['id'];
        familyModel.name = v['name'];
        familyModel.gender = v['gender'];
        familyModel.dateOfBirth = v['dateOfBirth'];
        familyModel.role = v['role'];
        _list.add(familyModel);
      }

      );
      emit(SuccessGetDataFamilyState(_list));
    } catch (e) {
      emit(FailedGetDataFamilyState("Terjadi kesalhan"));
    }
  }

  void postDataFamily(FamilyModel data)async{
    emit(LoadingPostDataFamilyState());

    try {
      await _service.saveFamily(data);
      emit(SuccessPostDataFamilyState());
    } catch (e) {
      emit(FailedPostDataFamilyState("Terjadi kesalahan."));
    }
  }

  void updateDataFamily(FamilyModel data)async{
    emit(LoadingPostDataFamilyState());

    try {
      await _service.updateDataFamily(data);
      emit(SuccessPostDataFamilyState());
    } catch (e) {
      emit(FailedPostDataFamilyState("Terjadi kesalahan."));
    }
  }
}
