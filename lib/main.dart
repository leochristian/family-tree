import 'package:family_tree/bloc/family_bloc.dart';
import 'package:family_tree/screens/form_family.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _blocFamily = FamilyBloc();

  @override
  void initState() {
    _blocFamily.getDataFamily();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Silsilah Keluarga"),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => FormFamily(
                        data: null,
                        reload: () => _blocFamily.getDataFamily(),
                      )));
        },
        child: Icon(Icons.add),
      ),
      body: SingleChildScrollView(
        child: BlocProvider(
          create: (context) => _blocFamily,
          child:
              BlocBuilder<FamilyBloc, FamilyState>(builder: (context, state) {
            if (state is LoadingGetDataFamilyState) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
            if (state is SuccessGetDataFamilyState) {
              if (state.data.length == 0) {
                return Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.4),
                  child: Center(
                    child: Text("Belum ada data."),
                  ),
                );
              } else {
                return Container(
                  child: Column(
                    children: state.data
                        .map((e) => Container(
                              width: double.infinity,
                              margin: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 16),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 15),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 1,
                                    blurRadius: 3,
                                    offset: Offset(
                                        0, 1), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    width: 35,
                                    height: 35,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: e.gender == "L"
                                            ? Colors.blue
                                            : Colors.red),
                                    child: Center(
                                        child: Icon(
                                      Icons.account_circle,
                                      color: Colors.white,
                                    )),
                                  ),
                                  SizedBox(
                                    width: 15,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        e.name,
                                        style: GoogleFonts.muli(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(e?.role ?? "-",
                                          style:
                                              GoogleFonts.muli(fontSize: 14)),
                                      Text(
                                          e.gender == "L"
                                              ? "Laki-laki"
                                              : "Perempuan",
                                          style:
                                              GoogleFonts.muli(fontSize: 14)),
                                      Text(DateFormat("dd-MMM-yyyy").format(DateTime.parse(e.dateOfBirth)),
                                          style:
                                              GoogleFonts.muli(fontSize: 14)),
                                    ],
                                  ),
                                  Spacer(),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (_) => FormFamily(
                                                    data: e,
                                                    reload: () => _blocFamily
                                                        .getDataFamily(),
                                                  )));
                                    },
                                    child: Icon(Icons.edit_sharp),
                                  )
                                ],
                              ),
                            ))
                        .toList(),
                  ),
                );
              }
            }

            if (state is FailedGetDataFamilyState) {
              return Center(
                child: Text(state.error),
              );
            }
            return Container();
          }),
        ),
      ),
    );
  }
}
