import 'package:family_tree/models/family_model.dart';
import 'package:family_tree/repositories/repository.dart';

class FamilyService {
  Repository _repository;

  FamilyService(){
    _repository = Repository();
  }

  saveFamily(FamilyModel data)async{
    return await _repository.insertData("family", data.familyMap());
  }

  readDataFamily() async{
   return await _repository.readDataFamily("family");
  }

  updateDataFamily(FamilyModel data)async{
    return await _repository.updateData("family", data.familyMap());
  }
}