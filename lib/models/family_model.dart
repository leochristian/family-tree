class FamilyModel {
  // id INTEGER PRIMARY KEY,name TEXT, gender TEXT, dateOfBirth TEXT, role TEXT, from TEXT
  int id;
  String name;
  String gender;
  String dateOfBirth;
  String role;
  String childOf; 

  FamilyModel({
    this.id,
    this.name,
    this.gender,
    this.dateOfBirth,
    this.role,
    this.childOf
  });

  familyMap(){
    var mapping = Map<String,dynamic>();
    mapping['id'] = id;
    mapping['name'] = name;
    mapping['gender'] = gender;
    mapping['dateOfBirth'] = dateOfBirth;
    mapping['role'] = role;
    mapping['childOf'] = childOf;

    return mapping;
  }
}