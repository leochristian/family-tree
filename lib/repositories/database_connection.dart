import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseConnection {
  
  setDatabase() async{
    var directory = await getApplicationDocumentsDirectory();
    var path = join(directory.path,"db_family_tree.db");
    var database = await openDatabase(path,version: 1,onCreate: _onCreatingDatabase);
    return database;
  }

  _onCreatingDatabase(Database database,int version)async{
    await database.execute("CREATE TABLE Family (id INTEGER PRIMARY KEY,name TEXT, gender TEXT, dateOfBirth TEXT, role TEXT, childOf TEXT)");
  }
}