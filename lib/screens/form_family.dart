import 'package:family_tree/bloc/family_bloc.dart';
import 'package:family_tree/models/family_model.dart';
import 'package:family_tree/widgets/dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class FormFamily extends StatefulWidget {
  final FamilyModel data;
  final Function reload;

  const FormFamily({Key key, this.data, this.reload}) : super(key: key);
  @override
  _FormFamilyState createState() => _FormFamilyState();
}

class _FormFamilyState extends State<FormFamily> {
  int id;
  String name, role, childOf, gender, dateOfBirth;
  int gen;

  bool loading = false;

  FamilyBloc _familyBloc = FamilyBloc();

  DateTime _dateTime = DateTime.now();

  @override
  void initState() {
    if (widget.data != null) {
      setState(() {
        id = widget.data.id;
        name = widget.data.name;
        role = widget.data.role;
        gender = widget.data.gender;
        childOf = widget.data.childOf;
        dateOfBirth = widget.data.dateOfBirth;
        _dateTime = DateTime.parse(widget.data.dateOfBirth);
        if (widget.data.gender == "L") {
          gen = 1;
        } else {
          gen = 2;
        }
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.data != null ? "Ubah Data" : "Tambah Data",
          style: GoogleFonts.muli(fontSize: 16, fontWeight: FontWeight.w700),
        ),
      ),
      bottomNavigationBar: InkWell(
        onTap: () {
          if (widget.data != null) {
            _familyBloc.updateDataFamily(FamilyModel(
                id: widget.data.id,
                name: name,
                gender: gender,
                dateOfBirth: _dateTime.toString(),
                role: role,
                childOf: null));
          } else {
            if (name == null) {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: "Nama harus diisi.",
                ),
              );
            } else if (gender == null) {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: "Jenis kelamin harap dipilih.",
                ),
              );
            } else if (_dateTime == null) {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: "Tanggal lahir harus diisi.",
                ),
              );
            } else if (role == null) {
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: "Status harap dipilih",
                ),
              );
            } else {
              _familyBloc.postDataFamily(FamilyModel(
                  name: name,
                  gender: gender,
                  dateOfBirth: _dateTime.toString(),
                  role: role,
                  childOf: null));
            }
          }
        },
        child: loading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 18),
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.blue),
                child: Center(
                  child: Text(
                    "Simpan",
                    style: GoogleFonts.muli(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
      ),
      body: BlocProvider(
        create: (context) => _familyBloc,
        child: BlocListener<FamilyBloc, FamilyState>(
          listener: (context, state) {
            if (state is LoadingPostDataFamilyState) {
              setState(() {
                loading = true;
              });
            }
            if (state is SuccessPostDataFamilyState) {
              setState(() {
                loading = false;
              });
              widget.reload();
              Navigator.pop(context);
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  title: widget.data != null ? "Perubahan berhasil disimpan.":"Berhasil disimpan.",
                ),
              );
            }
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 18),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                title("Nama"),
                TextFormField(
                  initialValue: name,
                  onChanged: (v) {
                    setState(() {
                      name = v;
                    });
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                title("Jenis Kelamin"),
                Row(
                  children: [
                    radio(1, "Laki-laki"),
                    radio(2, "Perempuan"),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                title("Status Dalam Keluarga"),
                DropdownButton(
                  isExpanded: true,
                  hint: Text("Pilih Status Dalam Keluarga"),
                  items: <DropdownMenuItem>[
                    DropdownMenuItem(
                      child: Text("Kakek"),
                      value: "Kakek",
                    ),
                    DropdownMenuItem(
                      child: Text("Nenek"),
                      value: "Nenek",
                    ),
                    DropdownMenuItem(
                      child: Text("Ayah"),
                      value: "Ayah",
                    ),
                    DropdownMenuItem(
                      child: Text("Ibu"),
                      value: "Ibu",
                    ),
                    DropdownMenuItem(
                      child: Text("Anak"),
                      value: "Anak",
                    ),
                    DropdownMenuItem(
                      child: Text("Cucu"),
                      value: "Cucu",
                    ),
                  ],
                  onChanged: (v) {
                    setState(() {
                      role = v;
                    });
                  },
                  value: role,
                ),
                SizedBox(
                  height: 15,
                ),
                title("Tanggal Lahir"),
                InkWell(
                  onTap: () async {
                    var date = await showDatePicker(
                      context: context,
                      initialDate: _dateTime,
                      firstDate: DateTime(1900),
                      lastDate: DateTime(2100),
                      builder: (BuildContext context, Widget child) {
                        return Theme(
                          data: ThemeData.light(),
                          child: child,
                        );
                      },
                    );
                    setState(() {
                      if (date != null) {
                        _dateTime = date;
                      }
                    });
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(width: 1, color: Colors.black45))),
                    child: Row(
                      children: [
                        Text(
                          DateFormat("dd-MMM-yyyy").format(_dateTime),
                          style: GoogleFonts.muli(
                            fontSize: 14,
                          ),
                        ),
                        Spacer(),
                        Icon(
                          Icons.calendar_today_outlined,
                          size: 16,
                          color: Colors.black54,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget title(String title) {
    return Text(
      title,
      style: GoogleFonts.muli(fontSize: 14, color: Colors.black45),
    );
  }

  Widget radio(int value, String title) {
    return Row(
      children: [
        Radio(
            activeColor: Colors.blue,
            value: value,
            groupValue: gen,
            onChanged: (v) {
              setState(() {
                gen = v;
                if (gen == 1) {
                  gender = "L";
                } else {
                  gender = "P";
                }
              });
            }),
        Text(
          title,
          textScaleFactor: 1,
          style: GoogleFonts.muli(fontSize: 14, color: Colors.black),
        )
      ],
    );
  }
}
